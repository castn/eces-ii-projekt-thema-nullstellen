def falsePosition(fct, a, b, corrVal):
    for i in range(100):
        c = a - (b - a) * fct(a) / (fct(b) - fct(a))  # Regula-falsi Formel

        if fct(a) * fct(c) < 0:
            b = c  # wenn f(a) * f(c) < 0
        else:
            a = c  # wenn f(a) * f(c) > 0

        if abs(c - corrVal) < 0.00000000000001:
            return i+1

    return c

from typing import *
import newton
import regulafalsi
import muller

CmpNumber = Union[float, complex]
Fct = Callable[[CmpNumber], CmpNumber]


def f_M_approx(x: CmpNumber) -> CmpNumber:
    return (4 / 75) * (x - (205 / 10)) ** 3 + (854 / 1000) * (x - (205 / 10)) ** 2 - (102 / 10) * (x - (205 / 10)) - (
                456 / 2)


def f_Q_approx(x: CmpNumber) -> CmpNumber:
    return 3 * (4 / 75) * (x - (205 / 10)) ** 2 + 2 * (854 / 1000) * (x - (205 / 10)) ** 1 - (102 / 10)


def f_q_approx(x: CmpNumber) -> CmpNumber:
    return 6 * (4 / 75) * (x - (205 / 10)) + 2 * (854 / 1000)


print("Newton-Verfahren")
itrsQ1 = newton.determine_root(f_Q_approx, 5, 5.558391178771452)
itrsQ2 = newton.determine_root(f_Q_approx, 25, 24.766608821228547)
itrsq = newton.determine_root(f_q_approx, 15, 15.1625)
print("Anzahl Iterationen: {}".format(itrsQ1))
print("Anzahl Iterationen: {}".format(itrsQ2))
print("Anzahl Iterationen: {}".format(itrsq))

print("Regula-falsi")
itrsQ1 = regulafalsi.falsePosition(f_Q_approx, float(-5), float(15), 5.558391178771452)
itrsQ2 = regulafalsi.falsePosition(f_Q_approx, float(15), float(35), 24.766608821228547)
itrsq = regulafalsi.falsePosition(f_q_approx, float(5), float(25), 15.1625)
print("Anzahl Iterationen: {}".format(itrsQ1))
print("Anzahl Iterationen: {}".format(itrsQ2))
print("Anzahl Iterationen: {}".format(itrsq))
print()

print("Mullerverfahren")
itrsQ1 = muller.mullerverfahren(f_Q_approx, (-5, 5, 15), 5.558391178771452)
itrsQ2 = muller.mullerverfahren(f_Q_approx, (15, 25, 35), 24.766608821228547)
itrsq = muller.mullerverfahren(f_q_approx, (5, 15, 25), 15.1625)
print("Anzahl Iterationen: {}".format(itrsQ1))
print("Anzahl Iterationen: {}".format(itrsQ2))
print("Anzahl Iterationen: {}".format(itrsq))
print()

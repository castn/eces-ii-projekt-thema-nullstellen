import numpy as np
from sympy import *

# Variable wird definiert.
x = Symbol('x')


# Approximationsalgorithmus zur Bestimmung der Nullstelle:
# 'fct' ist die untersuchte Funktion,
# 'init_val' ist der Anfangswert und
# 'corrVal' repraesentiert den gesuchten Wert
def determine_root(fct, init_val, corrVal):
    # Erste Ableitung von 'fct'.
    fct_derivative = fct.diff(x)

    xn = init_val
    for i in range(20):
        xn = xn - float(fct.evalf(subs={x: xn})) / float(fct_derivative.evalf(subs={x: xn}))
        if abs(xn - corrVal) < 0.0000000000001:
            return i + 1

    return xn


# Beispielfunktion:
print("Newton-Verfahren")
sample_fct = np.power(x, 2) - x - 2
fct_M = (4 / 75) * np.power(x - (205 / 10), 3) + (854 / 1000) * np.power(x - (205 / 10), 2) - (102 / 10) * (
            x - (205 / 10)) - (456 / 2)
fct_Q = 3 * (4 / 75) * np.power(x - (205 / 10), 2) + 2 * (854 / 1000) * (x - (205 / 10)) - (102 / 10)
fct_q = 6 * (4 / 75) * (x - (205 / 10)) + 2 * (854 / 1000)
itrsQ1 = determine_root(fct_Q, 5, 5.55839117877145)
itrsQ2 = determine_root(fct_Q, 25, 24.7666088212285)
itrsq = determine_root(fct_q, 15, 15.1625)
print("Anzahl Iterationen: {}".format(itrsQ1))
print("Anzahl Iterationen: {}".format(itrsQ2))
print("Anzahl Iterationen: {}".format(itrsq))
print()

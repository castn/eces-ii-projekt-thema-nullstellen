# ECES-II-Projekt Thema Nullstellen



Dieses Repository beinhaltet den kompletten Python Code, für das Praxisbeispiel unserer Hausarbeit zum Thema "Nullstellen" aus dem ECES-II Seminar an der TU-Darmstadt.

Die Implemenationen des Newton-, Muller- und Regula-falsi-Verfahren finden sich in den entsprechenden Dateien wieder. Zum berechnen der Werte, welche in der Hausarbeit verwendet wurden, muss die `main.py` ausgeführt. Folgende Python Packages müssen zu vor installiert werden: `typing, cmath, numpy, sympy`.

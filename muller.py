from typing import *
from cmath import sqrt

# Definieren von komplexen Zahlen und einer Funktion
CmpNumber = Union[float, complex]
Fct = Callable[[CmpNumber], CmpNumber]


# Ausrechnen einer dividierten Differenz
def divided_difference(f: Fct, xs: List[CmpNumber]):
    if len(xs) == 2:
        a, b = xs
        return (f(a) - f(b)) / (a - b)
    else:
        return (divided_difference(f, xs[1:]) - divided_difference(f, xs[0:-1])) / (xs[-1] - xs[0])


# Formel fuer das Mullerverfahren
def mullerverfahren(f: Fct, xs: (CmpNumber, CmpNumber, CmpNumber), corrVal: float) -> float:
    x0, x1, x2 = xs
    for i in range(100):
        w = divided_difference(f, (x2, x1)) + divided_difference(f, (x2, x0)) - divided_difference(f, (x2, x1))
        s_delta = sqrt(w ** 2 - 4 * f(x2) * divided_difference(f, (x2, x1, x0)))
        denominators = [w + s_delta, w - s_delta]

        x3 = x2 - 2 * f(x2) / max(denominators, key=abs)
        if abs(x3 - corrVal) < 0.00000000000001:
            return i + 1
        x0, x1, x2 = x1, x2, x3
    return x3
